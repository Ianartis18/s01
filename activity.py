

name = "Ian Stephen Artis"
age = 24
occupation = "Software Engineer"
movie = "Spidermen"
rating = 100

print(f"I am {name}, and I am {age} years old, I work a {occupation}, and my rating for {movie} is {float(rating)}%")


num1 = 10 
num2 = 20
num3 = 30

num2 += num3

# a
print(num1 * num2)
# b
print(num1 > num3)
# c
print(num2)
